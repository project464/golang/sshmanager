package main

import (
	"sshmanager/cmd/scripts"
	"sshmanager/cmd/servers"
	"sshmanager/database"
	"sshmanager/utils"

	"github.com/spf13/cobra"
	_ "modernc.org/sqlite"
)

func main() {
	var err error

	// Initialize the database.
	utils.DB, err = database.InitDatabase()
	if err != nil {
		panic(err)
	}
	defer utils.DB.Close()

	// Create the root command.
	rootCmd := &cobra.Command{
		Use:   "sshmanager",
		Short: "Manage your SSH servers",
	}
	var serversCmd = &cobra.Command{
		Use:   "server",
		Short: "Manage servers",
	}

	var scriptsCmd = &cobra.Command{
		Use:   "script",
		Short: "Manage scripts",
	}

	// Add the subcommands.
	rootCmd.AddCommand(serversCmd)
	rootCmd.AddCommand(scriptsCmd)
	serversCmd.AddCommand(servers.CreateCmd)
	serversCmd.AddCommand(servers.DeleteCmd)
	serversCmd.AddCommand(servers.ModifyCmd)
	serversCmd.AddCommand(servers.ListCmd)
	serversCmd.AddCommand(servers.ConnectCmd)
	serversCmd.AddCommand(servers.RunScriptCmd)
	scriptsCmd.AddCommand(scripts.CreateScriptCmd)
	scriptsCmd.AddCommand(scripts.ModifyScriptCmd)
	scriptsCmd.AddCommand(scripts.DeleteScriptCmd)
	scriptsCmd.AddCommand(scripts.ListScriptCmd)
	scriptsCmd.AddCommand(scripts.ShowScriptCmd)

	// Execute the command.
	if err := rootCmd.Execute(); err != nil {
		panic(err)
		//fmt.Errorf("Error:", err)
	}
}
