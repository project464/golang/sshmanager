package database

import (
	"database/sql"
	"log"
	"os"
	"path"
)

const folderName = ".sshmanager"
const dbName = "servers.db"

// Initialize the database.
func InitDatabase() (*sql.DB, error) {
	dbfilepath := folderCheck()
	db, err := sql.Open("sqlite", dbfilepath)
	if err != nil {
		log.Println("error opening file")
		return nil, err
	}

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS servers (
			id INTEGER PRIMARY KEY,
			name TEXT,
			address TEXT,
			username TEXT,
			password TEXT,
			key_path TEXT
		);
		CREATE TABLE IF NOT EXISTS scripts (
        	id INTEGER PRIMARY KEY,
        	name TEXT,
        	content TEXT
		);
	`)
	if err != nil {
		log.Println("error creating servers table")
		return nil, err
	}

	return db, nil
}

func folderCheck() string {
	// Проверяем существование папки
	home, err := os.UserHomeDir()
	if err != nil {
		panic(err)
	}
	dbFolder := path.Join(home, folderName)
	dbPath := path.Join(dbFolder, dbName)
	if _, err := os.Stat(dbFolder); os.IsNotExist(err) {
		// Если папка не существует, то создаём её
		err := os.Mkdir(dbFolder, 0755)
		if err != nil {
			panic(err)
		}
	}
	return dbPath
}
