package servers

import (
	"fmt"
)

type Server struct {
	ID       int64
	Name     string
	Address  string
	Username string
	Password string
	KeyPath  string
}

func (s *Server) String() string {
	return fmt.Sprintf("%d\t%s\t%s\t%s\t%s\t%s", s.ID, s.Name, s.Address, s.Username, s.Password, s.KeyPath)
}
