package servers

import (
	"log"
	"sshmanager/utils"
)

func GetServer(arg string) (*Server, error) {
	var server Server
	err := utils.DB.QueryRow(`
		SELECT id, name, address, username, password, key_path
		FROM servers
		WHERE id = ? OR name = ?
	`, arg, arg).Scan(
		&server.ID,
		&server.Name,
		&server.Address,
		&server.Username,
		&server.Password,
		&server.KeyPath,
	)
	if err != nil {
		log.Fatalf("Failed to query the database: %v", err)
		return nil, err
	}
	return &server, nil
}
