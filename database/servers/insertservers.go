package servers

import (
	"sshmanager/utils"
)

// Insert a new servers into the database.
func InsertServer(s *Server) (int64, error) {
	result, err := utils.DB.Exec(`INSERT INTO servers (name, address, username, password, key_path)
	VALUES (?, ?, ?, ?, ?)`, s.Name, s.Address, s.Username, s.Password, s.KeyPath)
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return id, nil
}
