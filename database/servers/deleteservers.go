package servers

import (
	"sshmanager/utils"
)

// Delete a servers from the database.
func DeleteServer(id int) (int64, error) {
	result, err := utils.DB.Exec("DELETE FROM servers WHERE id=?", id)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}
