package servers

import (
	"sshmanager/utils"
)

// Update a servers in the database.
func UpdateServer(s *Server) (int64, error) {
	result, err := utils.DB.Exec(`UPDATE servers SET name=?, address=?, username=?, password=?, key_path=?
	WHERE id=?`, s.Name, s.Address, s.Username, s.Password, s.KeyPath, s.ID)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}
