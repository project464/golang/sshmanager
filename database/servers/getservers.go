package servers

import (
	"sshmanager/utils"
)

// Get all servers from the database.
func GetServers() ([]Server, error) {
	rows, err := utils.DB.Query("SELECT id, name, address, username, password, key_path FROM servers")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var servers []Server
	for rows.Next() {
		var s Server
		err := rows.Scan(&s.ID, &s.Name, &s.Address, &s.Username, &s.Password, &s.KeyPath)
		if err != nil {
			return nil, err
		}
		servers = append(servers, s)
	}

	return servers, nil
}
