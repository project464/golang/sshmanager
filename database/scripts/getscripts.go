package scripts

import (
	"sshmanager/utils"
)

// Get all servers from the database.
func GetScripts() ([]Scripts, error) {
	rows, err := utils.DB.Query("SELECT id, name, content FROM scripts")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var scripts []Scripts
	for rows.Next() {
		var s Scripts
		err := rows.Scan(&s.ID, &s.Name, &s.ScriptContent)
		if err != nil {
			return nil, err
		}
		scripts = append(scripts, s)
	}

	return scripts, nil
}
