package scripts

import (
	"fmt"
)

type Scripts struct {
	ID            int64
	Name          string
	ScriptContent string
}

func (s *Scripts) String() string {
	return fmt.Sprintf("%d\t%s\n%s", s.ID, s.Name, s.ScriptContent)
}
