package scripts

import (
	"sshmanager/utils"
)

func DeleteScript(id int) (int64, error) {
	result, err := utils.DB.Exec("DELETE FROM scripts WHERE id=?", id)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}
