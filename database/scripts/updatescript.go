package scripts

import (
	"sshmanager/utils"
)

func UpdateScript(s *Scripts) (int64, error) {
	result, err := utils.DB.Exec("UPDATE scripts SET name=?, content=? WHERE id=?", s.Name, s.ScriptContent, s.ID)
	if err != nil {
		return 0, err
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return 0, err
	}

	return rowsAffected, nil
}
