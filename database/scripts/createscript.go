package scripts

import (
	"sshmanager/utils"
)

func AddScript(s *Scripts) (int64, error) {
	result, err := utils.DB.Exec(`INSERT INTO scripts(name, content) values(?, ?)`, s.Name, s.ScriptContent)
	if err != nil {
		return 0, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return 0, err
	}

	return id, nil
}
