package scripts

import (
	"log"
	"sshmanager/utils"
)

func GetScript(arg string) (*Scripts, error) {
	var script Scripts
	err := utils.DB.QueryRow(`
		SELECT id, name, content 
		FROM scripts
		WHERE id = ? OR name = ?
	`, arg, arg).Scan(
		&script.ID,
		&script.Name,
		&script.ScriptContent,
	)
	if err != nil {
		log.Fatalf("Failed to query the database: %v", err)
		return nil, err
	}
	return &script, nil
}
