package scripts

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"sshmanager/database/scripts"
)

var CreateScriptCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a new scripts",
	RunE: func(cmd *cobra.Command, args []string) error {
		name, _ := cmd.Flags().GetString("name")
		scriptfile, _ := cmd.Flags().GetString("file")

		if name == "" || scriptfile == "" {
			fmt.Printf("Error: You should set password (-p string) or keyPath (-k string) for servers.")
			return nil
		}
		scriptfileContent, err := os.ReadFile(scriptfile)
		if err != nil {
			fmt.Errorf("error reading file:", err)
		}

		s := &scripts.Scripts{
			Name:          name,
			ScriptContent: string(scriptfileContent),
		}

		id, err := scripts.AddScript(s)
		if err != nil {
			return err
		}

		fmt.Printf("Server created with ID %d\n", id)

		return nil
	},
}

func init() {
	CreateScriptCmd.Flags().StringP("name", "n", "", "script name (required)")
	CreateScriptCmd.Flags().StringP("file", "f", "", "script path(required)")
	CreateScriptCmd.MarkFlagRequired("name")
	CreateScriptCmd.MarkFlagRequired("file")
}
