package scripts

import (
	"fmt"
	"github.com/spf13/cobra"
	"sshmanager/database/scripts"
	"text/tabwriter"
)

var ListScriptCmd = &cobra.Command{
	Use:   "list",
	Short: "List all SSH servers",
	RunE: func(cmd *cobra.Command, args []string) error {
		scripts, err := scripts.GetScripts()
		if err != nil {
			return err
		}

		for _, s := range scripts {
			fmt.Printf("ID:%d\tName:%s\n", s.ID, s.Name)
			fmt.Println("Content:\n", s.ScriptContent)
		}
		w := tabwriter.NewWriter(cmd.OutOrStdout(), 0, 0, 3, ' ', 0)
		fmt.Fprintln(w, "ID\tName")
		for _, s := range scripts {
			fmt.Fprintf(w, "%d\t%s\n", s.ID, s.Name)
		}
		w.Flush()

		return nil
	},
}
