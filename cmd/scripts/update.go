package scripts

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"sshmanager/database/scripts"
	"strconv"
)

var ModifyScriptCmd = &cobra.Command{
	Use:   "modify",
	Short: "Modify an existing SSH servers",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		id, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}

		name, _ := cmd.Flags().GetString("name")
		script, _ := cmd.Flags().GetString("script")

		s := &scripts.Scripts{
			ID:            int64(id),
			Name:          name,
			ScriptContent: script,
		}
		log.Println("new record:", s)

		rowsAffected, err := scripts.UpdateScript(s)
		if err != nil {
			return err
		}

		if rowsAffected > 0 {
			fmt.Printf("Server with ID %d updated successfully\n", id)
		} else {
			fmt.Printf("Server with ID %d not found in the database\n", id)
		}

		return nil
	},
}

func init() {
	ModifyScriptCmd.Flags().StringP("name", "n", "", "script name")
	ModifyScriptCmd.Flags().StringP("script", "a", "", "script content")
}
