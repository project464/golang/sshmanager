package scripts

import (
	"fmt"
	"github.com/spf13/cobra"
	"sshmanager/database/scripts"
	"strconv"
)

var DeleteScriptCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete an existing scripts",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		id, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}

		rowsAffected, err := scripts.DeleteScript(id)
		if err != nil {
			return err
		}

		if rowsAffected > 0 {
			fmt.Printf("Server with ID %d deleted successfully\n", id)
		} else {
			fmt.Printf("Server with ID %d not found in the database\n", id)
		}

		return nil
	},
}
