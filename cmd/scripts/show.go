package scripts

import (
	"fmt"
	"github.com/spf13/cobra"
	"sshmanager/database/scripts"
)

var ShowScriptCmd = &cobra.Command{
	Use:   "show",
	Short: "Show script content",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		arg := args[0]
		s, err := scripts.GetScript(arg)
		if err != nil {
			return err
		}
		fmt.Printf("ID: %d - Name: %s\n", s.ID, s.Name)
		fmt.Println("\n\tContent:\n", s.ScriptContent)

		return nil
	},
}
