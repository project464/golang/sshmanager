package servers

import (
	"fmt"
	"sshmanager/database/servers"
	"text/tabwriter"

	"github.com/spf13/cobra"
)

var ListCmd = &cobra.Command{
	Use:   "list",
	Short: "List all SSH servers",
	RunE: func(cmd *cobra.Command, args []string) error {
		servers, err := servers.GetServers()
		if err != nil {
			return err
		}

		w := tabwriter.NewWriter(cmd.OutOrStdout(), 0, 0, 3, ' ', 0)
		fmt.Fprintln(w, "ID\tName\tAddress\tUsername\tPassword\tKeyPath")
		for _, s := range servers {
			if s.Password == "" {
				fmt.Fprintf(w, "%d\t%s\t%s\t%s\t\t%s\n", s.ID, s.Name, s.Address, s.Username, s.KeyPath)
			} else {
				fmt.Fprintf(w, "%d\t%s\t%s\t%s\t***\t\n", s.ID, s.Name, s.Address, s.Username)
			}
		}
		w.Flush()

		return nil
	},
}
