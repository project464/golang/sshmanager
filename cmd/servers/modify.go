package servers

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"sshmanager/database/servers"
	"strconv"
)

var ModifyCmd = &cobra.Command{
	Use:   "modify",
	Short: "Modify an existing SSH servers",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		id, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}

		name, _ := cmd.Flags().GetString("name")
		address, _ := cmd.Flags().GetString("address")
		username, _ := cmd.Flags().GetString("username")
		keyPath, _ := cmd.Flags().GetString("keypath")
		password, _ := cmd.Flags().GetString("password")

		if password == "" && keyPath == "" {
			fmt.Printf("Error: You should set password (-p string) or keyPath (-k string) for servers.")
			return nil
		}

		if password != "" && keyPath != "" {
			fmt.Printf("Error: You should choose password or key.")
			return nil
		}
		oldserver, err := servers.GetServer(args[0])

		s := &servers.Server{
			ID:       int64(id),
			Name:     name,
			Address:  address,
			Username: username,
			Password: password,
			KeyPath:  keyPath,
		}
		if s.Name == "" {
			s.Name = oldserver.Name
		}
		if s.Address == "" {
			s.Address = oldserver.Address
		}
		if s.Username == "" {
			s.Username = oldserver.Username
		}
		if s.Password == "" {
			s.Password = oldserver.Password
		}
		if s.KeyPath == "" {
			s.KeyPath = oldserver.KeyPath
		}

		log.Println("new record:", s)

		rowsAffected, err := servers.UpdateServer(s)
		if err != nil {
			return err
		}

		if rowsAffected > 0 {
			fmt.Printf("Server with ID %d updated successfully\n", id)
		} else {
			fmt.Printf("Server with ID %d not found in the database\n", id)
		}

		return nil
	},
}

func init() {
	ModifyCmd.Flags().StringP("name", "n", "", "servers name")
	ModifyCmd.Flags().StringP("address", "a", "", "servers address")
	ModifyCmd.Flags().StringP("username", "u", "", "username for SSH login")
	ModifyCmd.Flags().StringP("password", "p", "", "password for SSH login")
	ModifyCmd.Flags().String("keypath", "", "path to private key file")
}
