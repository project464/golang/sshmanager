package servers

import (
	"fmt"
	"github.com/spf13/cobra"
	"sshmanager/database/servers"
	"strconv"
)

var DeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete an SSH servers",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		id, err := strconv.Atoi(args[0])
		if err != nil {
			return err
		}

		rowsAffected, err := servers.DeleteServer(id)
		if err != nil {
			return err
		}

		if rowsAffected > 0 {
			fmt.Printf("Server with ID %d deleted successfully\n", id)
		} else {
			fmt.Printf("Server with ID %d not found in the database\n", id)
		}

		return nil
	},
}
