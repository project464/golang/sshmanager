package servers

import (
	"fmt"
	"sshmanager/database/servers"

	"github.com/spf13/cobra"
)

var CreateCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a new SSH servers",
	RunE: func(cmd *cobra.Command, args []string) error {
		name, _ := cmd.Flags().GetString("name")
		address, _ := cmd.Flags().GetString("address")
		username, _ := cmd.Flags().GetString("username")
		keyPath, _ := cmd.Flags().GetString("keypath")
		password, _ := cmd.Flags().GetString("password")

		if password == "" && keyPath == "" {
			fmt.Printf("Error: You should set password (-p string) or keyPath (-k string) for servers.")
			return nil
		}

		if password != "" && keyPath != "" {
			fmt.Printf("Error: You should choose password or key.")
			return nil
		}

		s := &servers.Server{
			Name:     name,
			Address:  address,
			Username: username,
			Password: password,
			KeyPath:  keyPath,
		}

		id, err := servers.InsertServer(s)
		if err != nil {
			return err
		}

		fmt.Printf("Server created with ID %d\n", id)

		return nil
	},
}

func init() {
	CreateCmd.Flags().StringP("name", "n", "", "servers name (required)")
	CreateCmd.Flags().StringP("address", "a", "", "servers address (required)")
	CreateCmd.Flags().StringP("username", "u", "", "username for SSH login (required)")
	CreateCmd.Flags().StringP("password", "p", "", "password for SSH login")
	CreateCmd.Flags().StringP("keypath", "k", "", "path to private key file")
	CreateCmd.MarkFlagRequired("name")
	CreateCmd.MarkFlagRequired("address")
	CreateCmd.MarkFlagRequired("username")
}
