package servers

import (
	"fmt"
	"github.com/spf13/cobra"
	"log"
	"sshmanager/database/servers"
	"sshmanager/sshclient"
)

var RunScriptCmd = &cobra.Command{
	Use:   "runscript",
	Short: "Execute scripts on remote servers by ID or name",
	Args:  cobra.ExactArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		script, _ := cmd.Flags().GetString("scripts")
		// Получение аргументов команды
		arg := args[0]

		// Поиск сервера в базе данных по ID или имени
		server, err := servers.GetServer(arg)
		if err != nil {
			log.Fatalf("Failed to query the database: %v", err)
			return err
		}

		// Подключение к серверу
		fmt.Printf("Connecting to servers %s (%s)...\n", server.Name, server.Address)
		// здесь должен быть код подключения по SSH с использованием полученных данных
		//sshclient.Connect(servers)
		sshclient.RunScript(server, script)
		return nil
	},
}

func init() {
	RunScriptCmd.Flags().StringP("scripts", "s", "", "scripts name (required)")
	RunScriptCmd.MarkFlagRequired("scripts")
}
