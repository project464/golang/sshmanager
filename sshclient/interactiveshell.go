package sshclient

import (
	"github.com/helloyi/go-sshclient"
	"golang.org/x/crypto/ssh"
	"log"
	"sshmanager/database/servers"
)

func InteractiveShell(server *servers.Server) {
	var client *sshclient.Client
	var err error
	if server.Password != "" {
		client, err = sshclient.DialWithPasswd(server.Address, server.Username, server.Password)
		if err != nil {
			log.Fatalf("error connect to host:", err)
		}
	} else {
		client, err = sshclient.DialWithKey(server.Address, server.Username, server.KeyPath)
		if err != nil {
			log.Fatalf("error connect to host:", err)
		}

	}
	defer client.Close()

	//client, err := sshclient.DialWithKeyWithPassphrase("host:port", "username", "prikeyFile", "my-passphrase"))
	//if err != nil {
	//	handleErr(err)
	//}
	//defer client.Close()

	// with a terminal config
	config := &sshclient.TerminalConfig{
		Term:   "xterm",
		Height: 40,
		Weight: 80,
		Modes: ssh.TerminalModes{
			ssh.ISIG:          50,
			ssh.TTY_OP_ISPEED: 14400, // input speed = 14.4kbaud
			ssh.TTY_OP_OSPEED: 14400, // output speed = 14.4kbaud
		},
	}
	if err := client.Terminal(config).Start(); err != nil {
		log.Fatalf("Unnable to run default terminal:", err)
	}

}
