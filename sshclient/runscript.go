package sshclient

import (
	"fmt"
	"github.com/helloyi/go-sshclient"
	"log"
	"os"
	"sshmanager/database/servers"
)

func RunScript(server *servers.Server, script string) {
	var client *sshclient.Client
	var err error
	if server.Password != "" {
		client, err = sshclient.DialWithPasswd(server.Address, server.Username, server.Password)
		if err != nil {
			log.Fatalf("error connect to host:", err)
		}
	} else {
		client, err = sshclient.DialWithKey(server.Address, server.Username, server.KeyPath)
		if err != nil {
			log.Fatalf("error connect to host:", err)
		}

	}
	defer client.Close()

	//client, err := sshclient.DialWithKeyWithPassphrase("host:port", "username", "prikeyFile", "my-passphrase"))
	//if err != nil {
	//	handleErr(err)
	//}
	//defer client.Close()

	// with a terminal config

	sc, err := os.ReadFile(script)
	if err != nil {
		log.Fatalf("Unnable to open file: %s", err)
	}

	output := client.Script(string(sc))
	if err != nil {
		log.Fatalf("Unnable to run scripts:", err)
	}
	str, err := output.Output()
	if err != nil {
		log.Fatalf("Unnable to run scripts:", err)
	}
	fmt.Println(string(str))

}
